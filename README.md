# COVID-19_infografias

Conjunto de infografías realizadas a partir de la iniciativa española **Coronavirus Makers**.

## Categorías ##

**Higiene**: infografías sobre prácticas de higiene en casa y fórmulas de limpieza (en base al criterio de la OMS).

**Nodos**: infografías sobre trabajo e higiene en los nodos de producción de material sanitario.